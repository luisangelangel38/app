from datetime import datetime

from django.db import models

class Type(models.Model):
    name = models.CharField(max_length=150, verbose_name='nombre',unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name='Tipo'
        verbose_name_plural= 'Tipos'
        ordering=['id']

class Category(models.Model):
    name= models.CharField(verbose_name='Nombre',max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name= 'Categoria'
        verbose_name_plural= 'Categorias'
        ordering=['id']

class Empleado(models.Model):
    type = models.ForeignKey(Type,on_delete=models.CASCADE)
    category= models.ManyToManyField(Category)
    nombres=models.CharField(max_length=200,verbose_name='Nombres',default='SIN NOMBRES')
    cedula=models.CharField(max_length=10,unique=True,verbose_name='Cedula')
    genero=models.CharField(max_length=30,verbose_name='Genero')
    #fecha_ingreso=models.DateField(default=datetime.date.,verbose_name='Fecha de ingreso')
    fecha_creacion= models.DateTimeField(auto_now=True)
    fecha_actualizada= models.DateTimeField(auto_now_add=True)
    age= models.PositiveIntegerField(verbose_name='Edad',default=0)
    sueldo= models.DecimalField(default=0.00,max_digits=9,decimal_places=2)
    estado= models.BooleanField(default=True)
    avatar= models.ImageField(upload_to='avatar/%Y/%m/%d',null=True,blank=True)

    def __str__(self):
        return self.nombres


    class Meta:
        verbose_name= 'Empleado'
        verbose_name_plural= 'Empleados'
        db_table='empleado'
        ordering=['id']


# Generated by Django 4.2.6 on 2023-10-28 03:36

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombres', models.CharField(default='SIN NOMBRES', max_length=200, verbose_name='Nombres')),
                ('cedula', models.CharField(max_length=10, unique=True, verbose_name='Cedula')),
                ('fecha_creacion', models.DateTimeField(auto_now=True)),
                ('fecha_actualizada', models.DateTimeField(auto_now_add=True)),
                ('age', models.PositiveIntegerField(default=0, verbose_name='Edad')),
                ('sueldo', models.DecimalField(decimal_places=2, default=0.0, max_digits=9)),
                ('estado', models.BooleanField(default=True)),
                ('avatar', models.ImageField(blank=True, null=True, upload_to='avatar/%Y/%m/%d')),
            ],
            options={
                'verbose_name': 'Empleado',
                'verbose_name_plural': 'Empleados',
                'db_table': 'empleado',
                'ordering': ['id'],
            },
        ),
    ]
